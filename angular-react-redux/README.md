
## Angular + React + Redux |* Rejected this option*
* Good for small size applications but not recommended for enterprise or large applications.
* Not easily maintainable because Libraries built for Angular don't support react and vice versa.
* To giving it a try we implemented Angular/Material but there is no luck to use both and make both frameworks at same stable version.
* You can give it a try by checking out latest **master** branch of our repo and head to **angular-react-redux** folder
* To run type these commands at angular-react-redux folder in your terminal

```
#!cmd

npm install
npm start
```