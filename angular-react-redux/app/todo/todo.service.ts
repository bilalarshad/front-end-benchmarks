import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Todo } from './todo';
import { Router } from '@angular/router';

@Injectable()
export class TodoService {
    private baseUrl: string = 'http://localhost/core/api';
    constructor(private http: Http, private router: Router) {
    }
    getAll(): Observable<Todo[]> {
        let x = this.http
            .get(`${this.baseUrl}/todo`, { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    }
    get(id: number): Observable<Todo> {
        let x = this.http.get(`${this.baseUrl}/todo/${id}`)
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    }
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
    save(todo: Todo): Observable<Response> {
        return this
            .http
            .put(`${this.baseUrl}/people/${todo.id}`, JSON.stringify(todo), { headers: this.getHeaders() });
    }

    getHeaders() {
        let headers = new Headers();
        this.ifNotAuthorizedThenRedirect(headers);
        headers.append('Accept', 'application/json');
        return headers;
    }

    ifNotAuthorizedThenRedirect(headers: Headers) {
        let token = localStorage.getItem('accessToken');
        if (token !== null || token !== '') {
            headers.append('Authorization', 'Bearer ' + token);
        }
    }

    mapTodo(response: Response): Todo {
        return this.toTodo(response.json());
    }

    mapTodos(response: Response): Todo[] {
        return response.json().results.map(this.toTodo);
    }

    toTodo(r: any): Todo {
        let todo = <Todo>({
            id: r.id,
            item: r.item,
            isCompleted: r.isCompleted
        });
        return todo;
    }

}
