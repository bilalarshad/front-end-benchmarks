"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Rx_1 = require("rxjs/Rx");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var TodoService = (function () {
    function TodoService(http, router) {
        this.http = http;
        this.router = router;
        this.baseUrl = 'http://localhost/core/api';
    }
    TodoService.prototype.getAll = function () {
        var x = this.http
            .get(this.baseUrl + "/todo", { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    };
    TodoService.prototype.get = function (id) {
        var x = this.http.get(this.baseUrl + "/todo/" + id)
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    };
    TodoService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    TodoService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Rx_1.Observable.throw(errMsg);
    };
    TodoService.prototype.save = function (todo) {
        return this
            .http
            .put(this.baseUrl + "/people/" + todo.id, JSON.stringify(todo), { headers: this.getHeaders() });
    };
    TodoService.prototype.getHeaders = function () {
        var headers = new http_1.Headers();
        this.ifNotAuthorizedThenRedirect(headers);
        headers.append('Accept', 'application/json');
        return headers;
    };
    TodoService.prototype.ifNotAuthorizedThenRedirect = function (headers) {
        var token = localStorage.getItem('accessToken');
        if (token !== null || token !== '') {
            headers.append('Authorization', 'Bearer ' + token);
        }
    };
    TodoService.prototype.mapTodo = function (response) {
        return this.toTodo(response.json());
    };
    TodoService.prototype.mapTodos = function (response) {
        return response.json().results.map(this.toTodo);
    };
    TodoService.prototype.toTodo = function (r) {
        var todo = ({
            id: r.id,
            item: r.item,
            isCompleted: r.isCompleted
        });
        return todo;
    };
    return TodoService;
}());
TodoService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], TodoService);
exports.TodoService = TodoService;
//# sourceMappingURL=todo.service.js.map