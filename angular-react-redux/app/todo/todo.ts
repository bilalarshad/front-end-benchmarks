import { Injectable } from '@angular/core';

@Injectable()
export class Todo {

    public id: number;

    public item: string;

    public isCompleted: boolean;

    constructor(item: string) {
        this.item = item;
    }
}
