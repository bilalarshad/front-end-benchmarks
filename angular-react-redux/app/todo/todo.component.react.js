"use strict";
var React = require("react");
var ReactDOM = require("react-dom");
var ReactTodo = React.createClass({
    getInitialState: function () {
        this.state = { todos: this.props.todos };
        return {
            'todos': this.props.todos
        };
    },
    render: function () {
        var elements = this.state.todos;
        return (React.createElement("div", null,
            React.createElement("h3", null, "Todo Items"),
            React.createElement("ul", { className: 'no-style-list' }, elements.map(function (item, i) {
                var flag = item.isCompleted;
                if (flag) {
                    return React.createElement("li", { key: i, className: 'no-style-list' },
                        React.createElement("input", { type: 'checkbox', checked: 'true' }),
                        React.createElement("label", null, item.Item));
                }
                else {
                    return React.createElement("li", { key: i, className: 'no-style-list' },
                        React.createElement("input", { type: 'checkbox' }),
                        React.createElement("label", null, item.Item));
                }
            }))));
    }
});
var ReactTodoView = (function () {
    function ReactTodoView() {
    }
    ReactTodoView.initialize = function (containerId, todos) {
        ReactDOM.render(React.createElement(ReactTodo, { todos: todos }), document.getElementById(containerId));
    };
    return ReactTodoView;
}());
exports.ReactTodoView = ReactTodoView;
//# sourceMappingURL=todo.component.react.js.map