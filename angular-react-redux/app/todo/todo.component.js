"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var todo_service_1 = require("./todo.service");
var todo_1 = require("./todo");
var todo_component_react_1 = require("./todo.component.react");
require("rxjs/add/operator/toPromise");
var TodoComponent = (function () {
    function TodoComponent(todoService, route, router) {
        this.todoService = todoService;
        this.route = route;
        this.router = router;
    }
    TodoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ifNotAuthorizedThenRedirect();
        this.todoService.getAll()
            .toPromise()
            .then(function (result) {
            _this.todos = result;
            todo_component_react_1.ReactTodoView.initialize('react-todo', _this.todos);
        });
    };
    TodoComponent.prototype.ifNotAuthorizedThenRedirect = function () {
        var token = localStorage.getItem('accessToken');
        if (token === null || token === '') {
            this.router.navigate(['home']);
        }
        else {
        }
    };
    return TodoComponent;
}());
TodoComponent = __decorate([
    core_1.Component({
        selector: 'todo',
        templateUrl: './app/todo/todo.component.html',
        styleUrls: ['./app/todo/todo.component.scss'],
        providers: [
            todo_1.Todo,
            todo_service_1.TodoService
        ]
    }),
    __metadata("design:paramtypes", [todo_service_1.TodoService,
        router_1.ActivatedRoute,
        router_1.Router])
], TodoComponent);
exports.TodoComponent = TodoComponent;
//# sourceMappingURL=todo.component.js.map