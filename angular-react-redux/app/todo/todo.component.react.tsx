import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Todo } from './todo';
interface ReactTodoViewProps {
	todos: Todo[];
}

let ReactTodo = React.createClass<ReactTodoViewProps, any>({
	getInitialState: function () {
		this.state = { todos: this.props.todos };
		return {
			'todos': this.props.todos
		};
	},
	render: function () {
		let elements = this.state.todos;
		return (
			<div>
				<h3>Todo Items</h3>
				<ul className='no-style-list'>
					{elements.map(function (item, i) {
						let flag = item.isCompleted;
						if (flag) {
							return <li key={i} className='no-style-list'>
								<input type='checkbox' checked='true'/>
								<label>{item.Item}</label>
							</li>;
						} else {
							return <li key={i} className='no-style-list'>
								<input type='checkbox' />
								<label>{item.Item}</label>
							</li>;
						}
					})}
				</ul>
			</div>
		);
	}
});

export class ReactTodoView {
	static initialize(containerId: string, todos: Todo[]) {
		ReactDOM.render(
			<ReactTodo todos={todos} />,
			document.getElementById(containerId));
	}
}
