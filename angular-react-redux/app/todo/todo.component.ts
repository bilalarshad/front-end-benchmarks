import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from './todo.service';
import { Todo } from './todo';
import { ReactTodoView } from './todo.component.react';
import 'rxjs/add/operator/toPromise';

@Component({
    selector: 'todo',
    templateUrl: './app/todo/todo.component.html',
    styleUrls: ['./app/todo/todo.component.scss'],
    providers: [
        Todo,
        TodoService
    ]
})
export class TodoComponent implements OnInit {
    todos: Todo[];
    sub: any;
    errorMessage: string;

    constructor(private todoService: TodoService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
        this.ifNotAuthorizedThenRedirect();
        this.todoService.getAll()
            .toPromise()
            .then(result => {
                this.todos = result;
                ReactTodoView.initialize('react-todo', this.todos);
            });
    }
    ifNotAuthorizedThenRedirect() {
        let token = localStorage.getItem('accessToken');
        if (token === null || token === '') {
            this.router.navigate(['home']);
        } else {
        }
    }
}
