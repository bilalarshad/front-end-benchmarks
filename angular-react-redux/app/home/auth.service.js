"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var AuthService = (function () {
    function AuthService(http, router) {
        this.http = http;
        this.router = router;
        this.authUrl = 'http://localhost/core/api/account';
    }
    AuthService.prototype.login = function (email, password) {
        var _this = this;
        var data = new http_1.URLSearchParams();
        data.append('email', email);
        data.append('password', password);
        try {
            var x = this.http
                .post(this.authUrl + "/login", data)
                .map(this.extractData)
                .catch(this.handleError);
            x.toPromise().then(function (result) {
                if (result.access_token !== null || result.access_token !== '') {
                    console.log(result);
                    localStorage.setItem('accessToken', result.access_token);
                    _this.router.navigate(['todo']);
                }
                else {
                }
            });
            return x;
        }
        catch (error) {
        }
    };
    AuthService.prototype.register = function () {
        var payload = 'Email=user%40local.com&ConfirmEmail=user%40local.com&Password=Liverpool%401&ConfirmPassword=Liverpool%401';
        var x = this.http
            .post(this.authUrl + "/login", JSON.parse(payload), { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    };
    AuthService.prototype.getHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Accept', 'application/json');
        return headers;
    };
    AuthService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AuthService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Rx_1.Observable.throw(errMsg);
    };
    return AuthService;
}());
AuthService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map