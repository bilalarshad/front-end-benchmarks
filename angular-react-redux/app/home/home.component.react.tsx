import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AuthService } from './auth.service';

interface ReactChildComponentViewProps {
	title: String;
	authService: AuthService;
}

let ReactChildComponent = React.createClass<ReactChildComponentViewProps, any>({
	getInitialState: function () {
		this.state = {
			email: '',
			password: ''
		};
		return {
			'email': '',
			'password': ''
		};
	},
	login: function () {
		this.props.authService.login(this.state.email, this.state.password);
	},
	userNameChanged(event) {
		this.setState({ email: event.target.value });
	},
	passwordChanged(event) {
		this.setState({ password: event.target.value });
	},
	render: function () {
		return (
			<div>
				<label htmlFor='email'>Email</label>
				<input name='email' type='text' value={this.state.email} onChange={this.userNameChanged} />
				<br />
				<label htmlFor='password'>Password</label>
				<input name='password' type='password' value={this.state.password} onChange={this.passwordChanged} />
				<br />
				<button onClick={this.login}>Login</button>
			</div>
		);
	}
});

export class ReactChildComponentView {
	static initialize(title, containerId: string, authService: AuthService) {
		ReactDOM.render(
			<ReactChildComponent title={title} authService={authService} />,
			document.getElementById(containerId));
	}
}
