import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Auth } from './auth';
import { Router } from '@angular/router';


@Injectable()
export class AuthService {
    private authUrl = 'http://localhost/core/api/account';
    constructor(private http: Http, private router: Router) {
    }

    login(email, password): Observable<any> {
        let data = new URLSearchParams();
        data.append('email', email);
        data.append('password', password);
        try {
            let x = this.http
                .post(`${this.authUrl}/login`, data)
                .map(this.extractData)
                .catch(this.handleError);
            x.toPromise().then(result => {
                if (result.access_token !== null || result.access_token !== '') {
                    console.log(result);
                    localStorage.setItem('accessToken', result.access_token);
                    this.router.navigate(['todo']);
                } else {
                }
            });

            return x;
        } catch (error) {
        }


    }
    register(): Observable<Auth[]> {
        let payload = 'Email=user%40local.com&ConfirmEmail=user%40local.com&Password=Liverpool%401&ConfirmPassword=Liverpool%401';
        let x = this.http
            .post(`${this.authUrl}/login`, JSON.parse(payload), { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
        return x;
    }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
}
