import { Injectable } from '@angular/core';
import * as uuid from 'node-uuid';


@Injectable()
export class Auth {

    public success: boolean;

    public data: Data;

    public message: string;

    public total: number;
}
export class Data {
    public access_token: string;
    public expires_in: number;
    public token_type: string;
}
