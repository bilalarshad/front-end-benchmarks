"use strict";
var React = require("react");
var ReactDOM = require("react-dom");
var ReactChildComponent = React.createClass({
    getInitialState: function () {
        this.state = {
            email: '',
            password: ''
        };
        return {
            'email': '',
            'password': ''
        };
    },
    login: function () {
        this.props.authService.login(this.state.email, this.state.password);
    },
    userNameChanged: function (event) {
        this.setState({ email: event.target.value });
    },
    passwordChanged: function (event) {
        this.setState({ password: event.target.value });
    },
    render: function () {
        return (React.createElement("div", null,
            React.createElement("label", { htmlFor: 'email' }, "Email"),
            React.createElement("input", { name: 'email', type: 'text', value: this.state.email, onChange: this.userNameChanged }),
            React.createElement("br", null),
            React.createElement("label", { htmlFor: 'password' }, "Password"),
            React.createElement("input", { name: 'password', type: 'password', value: this.state.password, onChange: this.passwordChanged }),
            React.createElement("br", null),
            React.createElement("button", { onClick: this.login }, "Login")));
    }
});
var ReactChildComponentView = (function () {
    function ReactChildComponentView() {
    }
    ReactChildComponentView.initialize = function (title, containerId, authService) {
        ReactDOM.render(React.createElement(ReactChildComponent, { title: title, authService: authService }), document.getElementById(containerId));
    };
    return ReactChildComponentView;
}());
exports.ReactChildComponentView = ReactChildComponentView;
//# sourceMappingURL=home.component.react.js.map