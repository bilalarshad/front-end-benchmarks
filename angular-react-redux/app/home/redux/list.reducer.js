"use strict";
var _ = require("underscore");
function reducer(state, action) {
    switch (action.type) {
        case 'ADD':
            return _.union(state, [action.value]);
        case 'REMOVE':
            return _.filter(state, function (item) { return item != action.value; });
        case 'REMOVE_LAST':
            return state.splice(0, state.length - 1);
        default:
            return state;
    }
}
exports.reducer = reducer;
//# sourceMappingURL=list.reducer.js.map