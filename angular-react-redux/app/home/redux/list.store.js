"use strict";
var redux_1 = require("redux");
var list_reducer_1 = require("./list.reducer");
var ValueStore = (function () {
    function ValueStore(values) {
        this.store = redux_1.createStore(list_reducer_1.reducer, values, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
    }
    Object.defineProperty(ValueStore.prototype, "values", {
        get: function () {
            return this.store.getState();
        },
        enumerable: true,
        configurable: true
    });
    ValueStore.prototype.dispatch = function (action) {
        this.store.dispatch(action);
    };
    return ValueStore;
}());
exports.ValueStore = ValueStore;
//# sourceMappingURL=list.store.js.map