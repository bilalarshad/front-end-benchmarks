"use strict";
function add(value) {
    if (value === void 0) { value = null; }
    return {
        type: 'ADD',
        value: value
    };
}
exports.add = add;
function remove(value) {
    return {
        type: 'REMOVE',
        value: value
    };
}
exports.remove = remove;
function removeLast() {
    return {
        type: 'REMOVE_LAST',
        value: null
    };
}
exports.removeLast = removeLast;
//# sourceMappingURL=list.action.js.map