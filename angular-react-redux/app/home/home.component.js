"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var home_component_react_1 = require("./home.component.react");
var list_store_1 = require("./redux/list.store");
var list_action_1 = require("./redux/list.action");
var home_1 = require("./home");
var auth_service_1 = require("./auth.service");
var router_1 = require("@angular/router");
var HomeComponent = (function () {
    function HomeComponent(model, injector, AuthService, router) {
        this.model = model;
        this.injector = injector;
        this.AuthService = AuthService;
        this.router = router;
        this.val = 'red';
        this.initStore();
    }
    HomeComponent.prototype.initStore = function () {
        this.store = new list_store_1.ValueStore(this.model.colors);
        this.store.store.subscribe(function () {
        });
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.ifNotAuthorizedThenRedirect();
        home_component_react_1.ReactChildComponentView.initialize(this.model.componentTitle, 'home-react', this.AuthService);
    };
    HomeComponent.prototype.onAdd = function () {
        this.store.dispatch(list_action_1.add(this.val));
    };
    HomeComponent.prototype.onRemove = function () {
        this.store.dispatch(list_action_1.remove(this.val));
    };
    HomeComponent.prototype.ifNotAuthorizedThenRedirect = function () {
        var token = localStorage.getItem('accessToken');
        console.log('token');
        console.log(token);
        if (token === null || token === '') {
            this.router.navigate(['home']);
        }
        else {
            this.router.navigate(['todo']);
        }
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        selector: 'home',
        templateUrl: './app/home/home.component.html',
        styleUrls: ['./app/home/home.component.scss'],
        providers: [
            home_1.Home,
            auth_service_1.AuthService
        ]
    }),
    __metadata("design:paramtypes", [home_1.Home, core_1.Injector, auth_service_1.AuthService, router_1.Router])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map