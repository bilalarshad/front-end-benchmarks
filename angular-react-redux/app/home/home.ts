import { Injectable } from '@angular/core';
import * as uuid from 'node-uuid';


@Injectable()
export class Home {

    public uuid: string;

    public title: string = 'Hello World Angular 4!';

    public componentTitle: string = 'Hello World React!';

    public colors: Array<string> = ['yellow', 'blue', 'orange', 'black', 'white'];

    public currentValue: string = 'Testing Value';

    constructor() {
        this.uuid = uuid.v4();
    }
}
