import { Component, OnInit, Injector } from '@angular/core';
import { ReactChildComponentView } from './home.component.react';
import { ValueStore } from './redux/list.store';
import { add, remove } from './redux/list.action';
import { Home } from './home';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'home',
	templateUrl: './app/home/home.component.html',
	styleUrls: ['./app/home/home.component.scss'],
	providers: [
		Home,
		AuthService
	]
})

export class HomeComponent implements OnInit {

	public store: ValueStore;

	public val: string = 'red';


	constructor(public model: Home, public injector: Injector, private AuthService: AuthService, private router: Router) {
		this.initStore();
	}

	public initStore() {
		this.store = new ValueStore(this.model.colors);
		this.store.store.subscribe(() => {
		});
	}

	public ngOnInit() {
		this.ifNotAuthorizedThenRedirect();
		ReactChildComponentView.initialize(this.model.componentTitle, 'home-react', this.AuthService);
	}

	public onAdd() {
		this.store.dispatch(add(this.val));
	}

	public onRemove() {
		this.store.dispatch(remove(this.val));
	}
	ifNotAuthorizedThenRedirect() {
		let token = localStorage.getItem('accessToken');
		console.log('token');
		console.log(token);
		if (token === null || token === '') {
			this.router.navigate(['home']);
		} else {
			this.router.navigate(['todo']);
		}
	}
}
