"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var uuid = require("node-uuid");
var Home = (function () {
    function Home() {
        this.title = 'Hello World Angular 4!';
        this.componentTitle = 'Hello World React!';
        this.colors = ['yellow', 'blue', 'orange', 'black', 'white'];
        this.currentValue = 'Testing Value';
        this.uuid = uuid.v4();
    }
    return Home;
}());
Home = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], Home);
exports.Home = Home;
//# sourceMappingURL=home.js.map