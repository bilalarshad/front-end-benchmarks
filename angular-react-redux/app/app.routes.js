"use strict";
var router_1 = require("@angular/router");
var home_component_1 = require("./home/home.component");
var todo_component_1 = require("./todo/todo.component");
var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'todo', component: todo_component_1.TodoComponent }
];
exports.routing = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routes.js.map