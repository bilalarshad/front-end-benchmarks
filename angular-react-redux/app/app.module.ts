import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent, DialogContent } from './app.component';
import { HomeComponent } from './home/home.component';
import { routing } from './app.routes';
import { TodoComponent } from './todo/todo.component';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    routing,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule.forRoot()
  ],
  declarations: [
    AppComponent, HomeComponent, TodoComponent,DialogContent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [

  ]
})
export class AppModule { }
