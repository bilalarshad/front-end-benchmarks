import React from 'react';
import { List, ListItem } from 'material-ui/List';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import ContentSend from 'material-ui/svg-icons/content/send';

import Home from 'material-ui/svg-icons/action/home';
import Subheader from 'material-ui/Subheader';
import Toggle from 'material-ui/Toggle';
import Divider from 'material-ui/Divider';

export default class ListExampleNested extends React.Component {

    state = {
        open: false,
    };

    handleToggle = () => {
        this.setState({
            open: !this.state.open,
        });
    };

    handleNestedListToggle = (item) => {
        this.setState({
            open: item.state.open,
        });
    };

    render() {
        return (
            <div>
                <br />
                <List>
                    <ListItem primaryText="TEST" leftIcon={<Home />} />
                    
                    <ListItem primaryText="Home" leftIcon={<Home />} />
                    <Divider />
                    <Subheader>Nested List Items</Subheader>


                    <ListItem primaryText="Sent mail" leftIcon={<ContentSend />} />
                    <ListItem primaryText="Drafts" leftIcon={<ContentDrafts />} />
                    <Divider />
                    <Subheader>Nested List Items</Subheader>
                    <ListItem primaryText="Sent mail" leftIcon={<ContentSend />} />
                    <ListItem primaryText="Drafts" leftIcon={<ContentDrafts />} />
                    <Divider />
                    <Subheader>Nested List Items</Subheader>
                    <ListItem primaryText="Sent mail" leftIcon={<ContentSend />} />
                    <ListItem primaryText="Drafts" leftIcon={<ContentDrafts />} />
                    <Divider />
                    <Subheader>Nested List Items</Subheader>
                    <ListItem primaryText="Sent mail" leftIcon={<ContentSend />} />


                </List>
            </div>
        );
    }
}