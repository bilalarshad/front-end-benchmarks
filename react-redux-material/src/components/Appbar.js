import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Menu from 'material-ui/svg-icons/navigation/menu';
import List from './List';
import Drawer from 'material-ui/Drawer';
import HorizontalLinearStepper from './Test';

class Login extends Component {
  static muiName = 'FlatButton';
  render() {
    return (
      <FlatButton {...this.props} label="Login" />
    );
  }
}

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

class Appbar extends Component {

  state = {
    logged: false,
    open: false
  };

  logged = () => this.setState({ logged: !this.state.logged, open: this.state.open });
  drawerChange = () => this.setState({ open: !this.state.open });

  render() {
    return (
      <div>
        <AppBar
          title="React App"
          iconElementLeft={<IconButton><Menu onClick={this.drawerChange} /></IconButton>}
          iconElementRight={this.state.logged ? <Logged /> : <Login onClick={this.logged} />}
          onClick={this.drawerChange}
        />
        <Drawer open={this.state.open}>
          <List></List>
        </Drawer>
        <HorizontalLinearStepper />
      </div>
    );
  }
}
export default Appbar;