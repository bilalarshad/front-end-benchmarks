import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

injectTapEventPlugin();

const AppbarStyles = () => getMuiTheme({
  palette: {
    primary1Color: '#2196F3'
  }
});
ReactDOM.render(
  <MuiThemeProvider muiTheme={AppbarStyles()}>
    <App />
  </MuiThemeProvider>,
  document.getElementById('root')
);
